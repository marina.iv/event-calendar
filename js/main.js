const TIME_ZONE = 'Europe/Kyiv';
const WEEK_DAYS = 7;
const MILLISECONDS_STRING_LENGTH = 13;

class Event {

	id;
	dateStart;
	dateEnd;
	duration;
	name;
	url;
	dayFullFormat;
	startDateTimestamp;
	endDateTimestamp;
	isRepeated;
	diffDuration;

	constructor(data) {

		this.id = data && data.id ? data.id : null;
		this.dateStart = data && data.dateStart ? data.dateStart : null;
		this.dateEnd = data && data.dateEnd ? data.dateEnd : null;
		this.duration = data && data.duration ? data.duration : 0;
		this.name = data && data.name ? data.name : '';
		this.url = data && data.url ? data.url : '';

		this.startDateTimestamp = this.dateStart ? this.getStartOfDayTimestamp(this.convertTimestampToCorrentFormat(this.dateStart)) : null;
		this.endDateTimestamp = this.startDateTimestamp && this.duration ? this.getEndDateTimestamp(this.startDateTimestamp, this.duration) : null;
		this.dayFullFormat = this.getDayFullFormat(this.startDateTimestamp);
		this.isRepeated = false;
		this.diffDuration = this.duration;
	}


	getStartOfDayTimestamp(timestamp) {
		return moment.tz(moment(timestamp).startOf('day'), TIME_ZONE).valueOf();
	}

	getEndDateTimestamp(timestamp,duration) {
		const totalDuration = 86400000 * duration; //86400000 - number of milliseconds in day
		return timestamp + totalDuration - 1;
	}

	getDayFullFormat(timestamp) {
		return moment(timestamp).format('DD-MM-YYYY').toString();
	}

	convertTimestampToCorrentFormat(timestamp) {
		const timestampStr = timestamp.toString().length > MILLISECONDS_STRING_LENGTH ? timestamp.toString().substring(0, MILLISECONDS_STRING_LENGTH) : timestamp.toString();
		const diff = MILLISECONDS_STRING_LENGTH - timestampStr.length;
		let multiplier = 1;

		if ( diff > 0 ) {
			let j = 0;

			while ( j < diff ) {
				multiplier = multiplier*10;
				j++;
			}
		}

		return +timestampStr*multiplier;
	}
}

class Day {

	id;
	startDateTimestamp;
	endDateTimestamp;
	dayShortFormat;
	dayFullFormat;
	weekday;
	events;

	constructor(timestamp, index) {
		this.id = index ? index : 0;
		this.startDateTimestamp = timestamp ? timestamp : null;
		this.endDateTimestamp = timestamp ? this.getEndOfDayTimestamp(timestamp) : null;
		this.dayShortFormat = timestamp ? this.getDayShortFormat(timestamp) : '';
		this.dayFullFormat = timestamp ? this.getDayFullFormat(timestamp) : '';
		this.weekday = timestamp ? this.getWeekday(timestamp) : '';
		this.events = [];
	}

	getEndOfDayTimestamp(timestamp) {
		return timestamp + 86400000 - 1;
	}

	getDayFullFormat(timestamp) {
		return moment(timestamp).format('DD-MM-YYYY').toString();
	}

	getDayShortFormat(timestamp) {
		return moment(timestamp).date().toString();
	}

	getWeekday(timestamp) {
		return moment(timestamp).isoWeekday();
	}

}

document.addEventListener('DOMContentLoaded', async() => {
	moment.locale('uk');

	const eventCalendarEl = document.getElementById('event-calendar');
	const todayTimestamp = moment.tz(moment().startOf('day'), TIME_ZONE).valueOf();

	let currentTimestamp = todayTimestamp;
	let eventsList = [];
	let monthByDayList = [];

	await init();

	async function init() {
		initEventCalendar(currentTimestamp);
	}

	async function initEventCalendar(timestamp) {
		document.querySelector('.js-calendar-spinner').classList.add('event-calendar-spinner--active');
		eventsList = await getEventsByTimestamp(timestamp);

		const daysInMonth = moment(timestamp).daysInMonth();
		const dayNumber = moment(timestamp).date();

		const firstDayOfMonthTimestamp = getDayTimestampWithSubtract(timestamp, dayNumber-1, 'days');
		const firstDayOfMonthWeekDayNumber = moment(firstDayOfMonthTimestamp).isoWeekday();
		const firstDayOfCalanderListTimestamp = firstDayOfMonthWeekDayNumber > 1 ? getDayTimestampWithSubtract(firstDayOfMonthTimestamp,firstDayOfMonthWeekDayNumber-1, 'days') : firstDayOfMonthTimestamp;

		const lastDayOfMonthTimestamp = getDayTimestampWithAdd(timestamp, daysInMonth-dayNumber, 'days');
		const lastDayOfMonthWeekDayNumber = moment(lastDayOfMonthTimestamp).isoWeekday();
		const lastDayOfCalenderListTimestamp = lastDayOfMonthWeekDayNumber < WEEK_DAYS ? getDayTimestampWithAdd(lastDayOfMonthTimestamp, WEEK_DAYS-lastDayOfMonthWeekDayNumber+1, 'days') : lastDayOfMonthTimestamp;

		let counter = firstDayOfCalanderListTimestamp;
		let i = 0;

		while( counter < lastDayOfCalenderListTimestamp ) {
			const _day = new Day(counter, i);

			eventsList.forEach(_event => {
				if ( _event.startDateTimestamp <= _day.startDateTimestamp 
					&& _event.endDateTimestamp >= _day.endDateTimestamp ) {
					const cloneEvent = Object.assign(new Event(_event), _event);
					cloneEvent.isRepeated = _event.startDateTimestamp !== _day.startDateTimestamp;
					cloneEvent.diffDuration = moment(_event.endDateTimestamp).diff(moment(_day.endDateTimestamp), 'days');
					
					_day.events.push(cloneEvent);
					_day.events.sort(( a, b) => {
						const durationA = a.duration;
						const durationB = b.duration;

						if (durationA > durationB) return 1;
						if (durationA === durationB) return 0;
						if (durationA < durationB) return -1;
					}).reverse();
				}
			});

			monthByDayList.push(_day);
			
			counter = getDayTimestampWithAdd(counter, 1, 'days');
			i++;
		}

		console.log(monthByDayList);
		drawCalendar(eventCalendarEl, monthByDayList);
		document.querySelector('.js-calendar-spinner').classList.remove('event-calendar-spinner--active');
	}


	function getDayTimestampWithAdd(timestamp, added, format) {
		return moment.tz(moment(timestamp).add(added, format).startOf('day'), TIME_ZONE).valueOf();
	}

	function getDayTimestampWithSubtract(timestamp, subtrahend, format) {
		return moment.tz(moment(timestamp).subtract(subtrahend, format).startOf('day'), TIME_ZONE).valueOf();
	}

	async function getEventsByTimestamp(timestamp) {
		// todo - remove timestampStr if bring timestamp format to the same form
		const timestampStr = timestamp.toString().length > 10 ? timestamp.toString().substring(0,10) : timestamp.toString();
		const eventsJson = await fetchJsonData(`https://kotygoroshko.mk.ua/wp-json/api-main/v1/events/${timestampStr}`);

		return mapEvents(eventsJson);
	}

	function mapEvents(eventsJson) {
		return eventsJson.map( _event => new Event(_event)).sort(compareByDate);
	}

	function compareByDate(a, b) {
		const timestampA = a.startDateTimestamp;
		const timestampB = b.startDateTimestamp;

		if (timestampA > timestampB) return 1;
		if (timestampA === timestampB) return 0;
		if (timestampA < timestampB) return -1;
	}

	async function fetchJsonData(url) {
		let data = [];
		const response = await fetch(url);

		if (response.ok) {
			data = await response.json();
		}

		return data;
	}

	function resetAll() {
		eventCalendarEl.innerHTML = '';
		eventsList = [];
		monthByDayList = [];
	}

	function drawCalendar(eventCalendarEl, monthByDayList) {

		const currentMonth = moment(currentTimestamp).format('MMMM YYYY');
		const prevMonth = moment(currentTimestamp).subtract(1, 'month').format('MMMM  YYYY');
		const nextMonth = moment(currentTimestamp).add(1, 'month').format('MMMM  YYYY');
		const todayStr = moment(todayTimestamp).format('DD.MM.YYYY').toString();
		const weekdaysList = moment.weekdays(true);

		// 1. create toolbar
		const toolbar_div = document.createElement('div');
		toolbar_div.classList.add('event-calendar__toolbar');

		// 2. create elements for toolbar
		const month_name_span = document.createElement('span');
		const prev_btn = document.createElement('span');
		const next_btn = document.createElement('span');
		const today_btn = document.createElement('span');

		month_name_span.classList.add('event-calendar__title');
		month_name_span.classList.add('event-calendar__toolbar-title');
		month_name_span.innerText = currentMonth;

		prev_btn.classList.add('event-calendar__btn', 'btn', 'btn-primary');
		prev_btn.classList.add('event-calendar__btn--prev');
		prev_btn.classList.add('js-event-calendar-prev-btn');
		prev_btn.innerText = prevMonth;

		next_btn.classList.add('event-calendar__btn', 'btn', 'btn-primary');
		next_btn.classList.add('event-calendar__btn--next');
		next_btn.classList.add('js-event-calendar-next-btn');
		next_btn.innerText = nextMonth;

		today_btn.classList.add('event-calendar__btn', 'btn', 'btn-danger');
		today_btn.classList.add('event-calendar__btn--today');
		today_btn.classList.add('js-event-calendar-today-btn');
		today_btn.innerText = todayStr;

		// 3. append elements into toolbar
		toolbar_div.append(prev_btn);
		toolbar_div.append(month_name_span);
		toolbar_div.append(next_btn);
		toolbar_div.append(today_btn);

		// 3. append toolbar to calendar parent element
		eventCalendarEl.append(toolbar_div);

		// 4. create grid div
		const grid_div = document.createElement('div');
		grid_div.classList.add('event-calendar__calendar-grid');

		// 5. append grid div to calendar parent element
		eventCalendarEl.append(grid_div);

		// 6. create header weekday row
		const header_row_div = document.createElement('div');
		header_row_div.classList.add('event-calendar__calendar-row');
		header_row_div.classList.add('event-calendar__calendar-row--header');

		// 7. create days col with weekdays name
		weekdaysList.forEach( (weekday, index) => {
			let div = document.createElement('div');
			div.classList.add('event-calendar__day');
			div.classList.add('event-calendar__day--weekday');
			div.innerText = weekday;

			header_row_div.append(div);
		});

		// 8. append header weekday row to grid
		grid_div.append(header_row_div);

		// 9. create row div for days / each row contain 7 days
		let weekday_row_div = document.createElement('div');
		weekday_row_div.classList.add('event-calendar__calendar-row');

		// 10. create day div
		monthByDayList.forEach( (day, index) => {
			const dayDiv = document.createElement('div');

			const titleSpan = document.createElement('span');
			titleSpan.innerText = day.dayShortFormat;
			titleSpan.classList.add('event-calendar__day__title');
			dayDiv.append(titleSpan);

			dayDiv.classList.add('event-calendar__day');

			if ( day.startDateTimestamp < todayTimestamp ) {
				dayDiv.classList.add('event-calendar__day--past');
			}

			if ( day.startDateTimestamp === todayTimestamp ) {
				dayDiv.classList.add('event-calendar__day--today');
			}

			const eventsDiv = document.createElement('div');
			eventsDiv.classList.add('event-calendar__day-events');

			// 11. fill day div with events
			day.events.forEach( (_event, index) => {
				const durationIn = _event.diffDuration + day.weekday;
				const crossCount = durationIn > WEEK_DAYS ? WEEK_DAYS - day.weekday : _event.diffDuration;
				let eventLink = document.createElement('a');
				eventLink.innerText = _event.name;
				eventLink.setAttribute('data-event-id', _event.id );
				eventLink.setAttribute('href', _event.url );
				eventLink.setAttribute('target', '_blank' );
				eventLink.classList.add('event-calendar__event');
				eventLink.classList.add(`event-calendar__event--cross-${ crossCount + 1 }`);

				if ( _event.isRepeated && day.weekday > 1 ) {
					eventLink.classList.add('event-calendar__event--repeated');
				}

				eventsDiv.append(eventLink);
			});

			dayDiv.append(eventsDiv);
			weekday_row_div.append(dayDiv);

			// 12. create new weekday row after week fill out with days
			if ( day.weekday === 7 ) {
				grid_div.append(weekday_row_div);
				weekday_row_div = document.createElement('div');
				weekday_row_div.classList.add('event-calendar__calendar-row');
			}
		});

		// 13. add listener on buttons
		document.querySelector('.js-event-calendar-prev-btn').addEventListener('click', (event) => {
			currentTimestamp = getDayTimestampWithSubtract(currentTimestamp, 1, 'month');
			resetAll();
			initEventCalendar(currentTimestamp);
		});

		document.querySelector('.js-event-calendar-next-btn').addEventListener('click', (event) => {
			currentTimestamp = getDayTimestampWithAdd(currentTimestamp, 1, 'month');
			resetAll();
			initEventCalendar(currentTimestamp);
		});

		document.querySelector('.js-event-calendar-today-btn').addEventListener('click', (event) => {
			currentTimestamp = todayTimestamp;
			resetAll();
			initEventCalendar(currentTimestamp);
		});
	}

});